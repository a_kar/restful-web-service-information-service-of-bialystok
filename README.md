# README #

Projekt RESTful Web Services
Temat: Serwis informacyjny Białegostoku

### Wymagania funkcjonalne:###
1. Pobieranie eventów dla danego dnia
2. Pobieranie eventów dla danego tygodnia
3. Pobranie informacji o danym evencie
4. Dodawanie eventów
5. Modyfikacja informacji o danym evencie
6. Usuwanie eventów
7. Odbiór zestawienia eventów w formacie PDF

### Przeznaczenie ###
Celem tego dokumentu jest przedstawienie zasad wymiany informacji pomiędzy RESTful
web service serwerem, a oprogramowaniem systemów klienckich, pobierających informacje
dotyczące wydarzeń w Białymstoku oraz umożliwiających ich modyfikację i dodawanie.
Opracowanie przeznaczone jest dla osób i firm z branży IT przygotowujących samodzielnie
oprogramowanie interfejsowe.


### Specyfikacja usługi ###
Serwis zaimplementowany został jako RESTful web service server.
Usługa dostępna jest poprzez protokół HTTP
Projekt po wdrożeniu na serwerze Glassfish jest dostępny pod adresem:
*http://localhost:8080//RestEvents*
Do korzystania z projektu wymagana jest autoryzacja:
*Typ: Basic Auth
Username: “user123”
Password: “password”
*