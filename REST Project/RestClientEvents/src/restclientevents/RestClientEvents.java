/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restclientevents;

import java.io.IOException;
import java.net.ProxySelector;
import java.util.Base64;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;



public class RestClientEvents {

    
//    public static void main(String[] args) {
//        
////        Client client = ClientBuilder.newClient().register(new Authenticator("user123", "password"));
////        WebTarget target = client.target("http://localhost:8080/RestEvents/webresources/events/3");
////        String message = target.request(MediaType.APPLICATION_JSON).get(String.class);
////        System.out.println("response: " + message);
//        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("username", "password");
//
//        final Client client = ClientBuilder.newClient();
//        client.register(feature);
//        
//        
//
//    }
public static void main(String[] args) throws IOException 
{
    ProxySelector.setDefault(new CustomProxySelector());
    httpGETCollectionExample();
}
 
private static void httpGETCollectionExample() 
{
    ClientConfig clientConfig = new ClientConfig();
 
    HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("user123", "password");
    clientConfig.register( feature) ;
 
 
    Client client = ClientBuilder.newClient( clientConfig );
    WebTarget webTarget = client.target("http://localhost:8080/RestEvents/webresources/events/");
     
    Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
    Response response = invocationBuilder.get();
     
    System.out.println(response.getStatus());
    System.out.println(response.getStatusInfo());
    
    String message = webTarget.request(MediaType.APPLICATION_JSON).get(String.class);
    System.out.println("response: " + message);
     
    if(response.getStatus() == 200)
    {
        
        System.out.println("200!");
    }
}
   
    
}
