/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.URI;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.FileDataSource;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import resources.EventService;
import model.Event;
import filter.MyResponseFilter;
import java.util.StringTokenizer;
import javax.annotation.security.RolesAllowed;
import org.apache.xml.security.exceptions.Base64DecodingException;
import org.apache.xml.security.utils.Base64;


@Path("events")
public class EventResource  {

    EventService eventService = new EventService(); 
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Event> getAllEventsJson() {        
        return eventService.getAllEvents();
    }
    
    @GET
    @Path("/{eventId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Event getEvent(@PathParam("eventId") Long id, @Context UriInfo uriInfo) {
        Event newEvent = eventService.getEvent(id);
        String uri = uriInfo.getBaseUriBuilder()
                .path(EventResource.class)
                .path(String.valueOf(newEvent.getId()))
                .build()
                .toString();
        
        newEvent.addLink(uri, "self");
        return newEvent;
    }
    
    @PUT
    @Path("/{eventId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Event updateEvent(@PathParam("eventId") Long id, Event event) {
        return eventService.updateEvent(event, id);
    }
    
    @DELETE
    @Path("/{eventId}")
    public void deleteEvent(@PathParam("eventId") Long id) {
        eventService.deleteEvent(id);
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createEvent(Event event, @Context UriInfo uriInfo) {
        Event newEvent = eventService.createEvent(event);
        String newId = String.valueOf(newEvent.getId());
        URI uri = uriInfo.getAbsolutePathBuilder().path(newId).build();
        Response response = Response.created(uri)
                .entity(newEvent)
                .build();
        return response;
    }
    
//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    public List<Event> getAllEventsStartingWith(@QueryParam("nazwa") String par1) {
//        if (par1 != null){
////            return eventService.getAllEventsStartingWith(par1);
//return eventService.getAllEvents();
//        }
//        return eventService.getAllEvents();
//    }

//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    public List<Event> getAllEventsInWeek(@QueryParam("week") int week) {
//        if (week != 0){
//            return eventService.getAllEventsInWeek(week);
//        }
//        return eventService.getAllEvents();
//    }
//    
//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    public List<Event> getAllEventsInWeek(@QueryParam("week") int week) {
//        return eventService.getAllEvents();
//    }
    
    @Path("/week")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Event> getEventsInWeek(@MatrixParam("week") int week) {
        if (week != 0){
            return eventService.getAllEventsInWeek(week);
        }
        return eventService.getAllEvents();
    }
    
    
    
    @Path("/month")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Event> getEventsInMonth(@MatrixParam("month") int month) {
                
        if (month != 0){
            return eventService.getAllEventsInMonth(month);
        }
        return eventService.getAllEvents();
    }
    
    @Path("/date")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Event> getEventsInDate(@MatrixParam("date") String date) {
        if (date != null){
            return eventService.getAllEventsInDate(date);
        }
        return eventService.getAllEvents();
    }
    
    @Path("/name")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Event> getAllEventsNamed(@MatrixParam("name") String name) {
        if (name != null){
            return eventService.getAllEventsNamed(name);
        }
        return eventService.getAllEvents();
    }
    
    @GET  
    @Path("/getpdf")  
    @Produces("application/pdf")    
    public Response getFile() { 
        
        eventService.writeUsingIText();
        String filepath = "C:\\Users\\alicj\\OneDrive\\Pulpit\\Events_zestawienie.pdf";
        File file = new File(filepath);  
        ResponseBuilder response = Response.ok((Object) file); 
        response.header("Content-Disposition","attachment; filename=\"Events_zestawienie.pdf\"");
        
        return response.build();  
   
    } 

    
    
    
    
    
}
