package filter;

import java.io.IOException;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import org.apache.xml.security.exceptions.Base64DecodingException;
import org.apache.xml.security.utils.Base64;

@Provider
public class MyResponseFilter implements ContainerRequestFilter, ContainerResponseFilter {
    
    
    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        final List<String> authorization = requestContext.getHeaders().get("Authorization");
        final String encodedUserPassword = authorization.get(0).replaceFirst("Basic" + " ", "");
         
        String usernameAndPassword;
        try {
            usernameAndPassword = new String(Base64.decode(encodedUserPassword.getBytes()));
            final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
            final String username = tokenizer.nextToken();
            final String password = tokenizer.nextToken();            
            
            System.out.println("Auth: ");
            System.out.println("USERNAME: " + username);
            System.out.println("PASSWORD: " + password);
            System.out.println("Is User Authenticated?: " + isUserAuthenticated(requestContext));
            
            if(!isUserAuthenticated(requestContext)){
                Logger.getLogger(MyResponseFilter.class.getName()).log(Level.SEVERE, null, "unathorized");
                requestContext.abortWith(Response.status(401).build());
            }
            
        } catch (Base64DecodingException ex) {
            Logger.getLogger(MyResponseFilter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean isUserAuthenticated(ContainerRequestContext requestContext) throws IOException {
        
        final List<String> authorization = requestContext.getHeaders().get("Authorization");
        final String encodedUserPassword = authorization.get(0).replaceFirst("Basic" + " ", "");
         
        String usernameAndPassword;
        try {
            usernameAndPassword = new String(Base64.decode(encodedUserPassword.getBytes()));
            final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
            final String username = tokenizer.nextToken();
            final String password = tokenizer.nextToken(); 
            
            if(username.equals("user123") && password.equals("password"))
            {
                return true;
            }
        } catch (Base64DecodingException ex) {
            Logger.getLogger(MyResponseFilter.class.getName()).log(Level.SEVERE, null, ex);
        }        
        return false;
    }
    
    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        responseContext.getHeaders().add("mojNaglowek", "rsi test");
    }
}
