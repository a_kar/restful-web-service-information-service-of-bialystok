/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.URI;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Event;
import javax.ws.rs.core.UriInfo;
import filter.MyResponseFilter;


public class EventService {
     
    static private Map<Long, Event> events = new HashMap<Long, Event>();
    
    public EventService() {        
        Event e1 = new Event(1L, "Balet", "kulturalne", "Najbardziej znane klasyki w wykonaniu teatru baletowego.", "12-05-2021", 19, 5, 2021);
        events.put(1L, e1);
        Event e2 = new Event(2L, "Mecz", "sportowe",  "Mecz na stadionie miejskim, walka o mistrzostwa.", "22-05-2021", 20, 5, 2021);
        events.put(2L, e2);
        Event e3 = new Event(3L, "Koncert", "kulturalne", "Niezwykly koncert skrzypcowy w filharmonii.", "22-05-2021", 20, 5, 2021 );
        events.put(3L, e3);
        Event e4 = new Event(4L, "Wystawa", "artystyczne", "Otwarcie wystawy obrazow impresjonistycznych.", "12-05-2021", 19, 5, 2021);
        events.put(4L, e4);
        
    }
            
    public List<Event> getAllEvents() {
        return new ArrayList<Event>(events.values());
    }
    
    public Event getEvent(Long id){
        System.out.println(events.get(id).getName());
        return events.get(id);        
    }
    
    public Event createEvent(Event event) {
        event.setId(events.size() + 1L);
        events.put(events.size() + 1L, event);
        return event;
    }
    

    public Event updateEvent(Event event, Long id) {
        Long event_id = events.get(id).getId();
        event.setId(event_id);
        events.put(event_id, event);
        return event;
    }

    public void deleteEvent(Long id) {
        events.remove(id);
    }
    
    public List<Event> getAllEventsInWeek(int week) {
        
        System.out.println("getAllEventsInWeek: "+String.valueOf(week));
        List<Event> list = new ArrayList<Event>();
        for (Event e : events.values()) {
            if(e.getWeekNumber() == week) {
                list.add(e);
            }
        }
        return list;
    }
    
    public List<Event> getAllEventsInMonth(int month) {
        System.out.println("getAllEventsInMonth: "+String.valueOf(month));
        List<Event> list = new ArrayList<Event>();
        for (Event e : events.values()) {
            if(e.getMonthNumber() == month) {
                list.add(e);
            }
        }
        return list;
    }
    
    public List<Event> getAllEventsInDate(String date) {
        System.out.println("getAllEventsInDate: "+String.valueOf(date));
        List<Event> list = new ArrayList<Event>();
        for (Event e : events.values()) {
            if(e.getDate().equals(date)) {
                list.add(e);
            }
        }
        return list;
    }
    
    public List<Event> getAllEventsNamed(String name) {
        System.out.println("getAllEventsInDate: "+String.valueOf(name));
        List<Event> list = new ArrayList<Event>();
        for (Event e : events.values()) {
            if(e.getName().equals(name)) {
                list.add(e);
            }
        }
        return list;
    }    
        
    public void writeUsingIText() {

        String filename = "C:\\Users\\alicj\\OneDrive\\Pulpit\\Events_zestawienie.pdf";
        Document document = new Document();

        try {

            PdfWriter.getInstance(document, new FileOutputStream(new File(filename)));

            //open
            document.open();
            
            Font f = new Font();
            f.setStyle(Font.BOLDITALIC);
            f.setSize(18);

            String title = "Zestawienie\n\n";                
            document.add(new Paragraph(title, f));
            document.addTitle(title);
                        
            String info = "";
            String name = "";           
           
            for (Event e : events.values()) {         
                
                f.setStyle(Font.BOLD);
                f.setSize(12);
                
                name += "Nazwa: " +e.getName()+ "\n";                
                document.add(new Paragraph(name, f));
                name = "";
                
                f.setStyle(Font.NORMAL);
                f.setSize(10);
                
                
                info += "Typ wydarzenia: "+ e.getType() + "\n"; 
                info += "Data: "+ e.getDate() + "\n";
                info += "Tydzien roku: "+ String.valueOf(e.getWeekNumber());
                info += "\t | Miesiac: " + String.valueOf(e.getMonthNumber());
                info += "\t | Rok: " + String.valueOf(e.getYearNumber() + "\n");
                info += "Opis: "+ e.getDescription() + "\n\n";
                
                document.add(new Paragraph(info, f));                
                info = "";
            } 

            //close
            document.close();

            System.out.println("New PDF file is done.");
         
        } catch (FileNotFoundException | DocumentException e) {
            e.printStackTrace();
        } 
    }
    
    
}
