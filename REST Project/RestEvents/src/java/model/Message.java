/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author alicj
 */
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Link;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Message {
    
    private long id;
    private String message;    
    @XmlElement 
    private Date created;    
    private String author;
    @XmlElement 
    private List<Link> links = new ArrayList<Link>();

    
    public Message(){        
    }
    
    public Message(long id, String message, String author) {
        this.id = id;
        this.message = message;
        this.created = new Date();
        this.author = author;
    }
    
    public long getID() {
        return id;
    }
    
    public void setID(long id) {
        this.id = id;
    }
    
    public String getMessage() {
        return message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    
    public Date getCreated() {
        return created;
    }
    
    public void setCreated(Date created) {
        this.created = created;
    }
    
    public String getAuthor() {
        return author;
    }
    
    public void setAuthor(String author) {
        this.author = author;
    }

    public void addLink(String uri, String name) {
        Link newLink = new Link();
        newLink.setLink(uri);
        newLink.setRel(name);
        links.add(newLink);
    }
    
    public List<Link> getLinks() {
        return links;
    }
}
