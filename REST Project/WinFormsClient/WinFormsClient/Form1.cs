﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        

        private void cmdGO_Click(object sender, EventArgs e)
        {
            RestClient rClient = new RestClient(httpVerb.GET);
            rClient.endPoint = textRestURI.Text;            

            debugOutput(Environment.NewLine + "Rest Client created.");

            string strResponse = string.Empty;
            string username = textUsername.Text; // "user123";
            string password = textPassword.Text; // "password";
            strResponse = rClient.makeRequest(username, password, "");

            if (rClient.endPoint == "http://localhost:8080/RestEvents/webresources/events/getpdf")
            {
                    strResponse = "Pobrano zestawienie PDF" +
                    Environment.NewLine +
                    "Zapisano plik: C:\\Users\\alicj\\OneDrive\\Pulpit\\Events_zestawienie.pdf";
            }

            debugOutput(strResponse);
        }


        private void debugOutput(string strDebugText)
        {
            try
            {
                System.Diagnostics.Debug.Write(strDebugText + Environment.NewLine);
                textResponse.Text = textResponse.Text + strDebugText + Environment.NewLine;
                textResponse.SelectionStart = textResponse.TextLength;
                textResponse.ScrollToCaret();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write(ex.Message, ToString() + Environment.NewLine);
            }
        }

        private void cmdPOST_Click(object sender, EventArgs e)
        {
            RestClient rClient = new RestClient(httpVerb.POST);
            rClient.endPoint = textRestURI.Text;

            debugOutput(Environment.NewLine + "Rest Client created.");

            string strResponse = string.Empty;
            string username = textUsername.Text; // "user123";
            string password = textPassword.Text; // "password";
            string postData = textContent.Text;
            strResponse = rClient.makeRequest(username, password, postData);

            debugOutput(strResponse);
        }

        private void cmdPUT_Click(object sender, EventArgs e)
        {
            RestClient rClient = new RestClient(httpVerb.PUT);
            rClient.endPoint = textRestURI.Text;

            debugOutput(Environment.NewLine + "Rest Client created.");

            string strResponse = string.Empty;
            string username = textUsername.Text; // "user123";
            string password = textPassword.Text; // "password";
            string postData = textContent.Text;
            strResponse = rClient.makeRequest(username, password, postData);

            debugOutput(strResponse);
        }

        private void cmdDELETE_Click(object sender, EventArgs e)
        {
            RestClient rClient = new RestClient(httpVerb.DELETE);
            rClient.endPoint = textRestURI.Text;

            debugOutput(Environment.NewLine + "Rest Client created.");

            string strResponse = string.Empty;
            string username = textUsername.Text; // "user123";
            string password = textPassword.Text; // "password";
            string postData = "";
            strResponse = rClient.makeRequest(username, password, postData);

            debugOutput(strResponse);
        }
    }
}
