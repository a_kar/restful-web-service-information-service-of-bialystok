﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;

namespace WinFormsClient
{

    public enum httpVerb
    {
           GET,
           POST,
           PUT,
           DELETE
    }
    class RestClient
    {
        public string endPoint { get; set; }
        public httpVerb httpMethod { get; set; }

        public RestClient(httpVerb action)
        {
            endPoint = string.Empty;
            httpMethod = action;
        }

        public string makeRequest(string username, string password, string postData)
        {
            string strResponseValue = string.Empty;
            
            string encoded = System.Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1")
                                           .GetBytes(username + ":" + password));            

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(endPoint);



            request.Headers.Add("Authorization", "Basic " + encoded);

            request.Method = httpMethod.ToString();

            if (request.Method == "POST")
            {
                request.ContentType = "application/json";
                //string postData = "This is a test that posts this string to a Web server.";
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                // Set the ContentLength property of the WebRequest.
                request.ContentLength = byteArray.Length;

                // Get the request stream.
                Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.
                dataStream.Close();
            }


            if (request.Method == "PUT")
            {
                request.ContentType = "application/json";
                //string postData = "This is a test that posts this string to a Web server.";
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                // Set the ContentLength property of the WebRequest.
                request.ContentLength = byteArray.Length;

                // Get the request stream.
                Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.
                dataStream.Close();
            }

            

            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        //throw new ApplicationException("error code: " + response.StatusCode.ToString());
                        strResponseValue = "Status code: " + response.StatusCode.ToString();
                    }
                    else
                    {
                        using (Stream responseStream = response.GetResponseStream())
                        {
                            if (responseStream != null)
                            {
                                using (StreamReader reader = new StreamReader(responseStream))
                                {
                                    strResponseValue = reader.ReadToEnd();
                                }
                            }
                        }
                    }


                }
            }
            catch(Exception ex)
            {
                strResponseValue = ex.Message;
            }
            
            return strResponseValue;
        }

    }
}
